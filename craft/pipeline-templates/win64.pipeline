// Sometimes we need to include additional parameters to Craft which are specific to the project
def craftProjectParams = ""

// Where will the craftmaster configuration be found (relative to the working directory that is)?
def craftmasterConfig = "binary-factory-tooling/craft/configs/master/CraftBinaryCache.ini"

// Determine if we need to build a specific version of this project
if( craftTarget != '' ) {
	// If we have a specified version (Craft target) then we need to pass this along to Craft
	craftProjectParams = "--target ${craftTarget}"
}

// Request a node to be allocated to us
node( "WindowsMSVC" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First we want to make sure Craft is ready to go
		stage('Preparing Craft') {
			// Make sure we start with a clean slate
			deleteDir()

			// Grab our tooling which we will need in a few moments
			checkout changelog: false, poll: false, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'bf-tooling/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/sysadmin/binary-factory-tooling.git']]
			]

			// Make sure that Craftmaster is up to date
			bat """
				python "%WORKSPACE%\\bf-tooling\\craft\\checkout-repository.py" --repository https://invent.kde.org/packaging/craftmaster.git --into "C:/Craft/BinaryFactory/"
				python "%WORKSPACE%\\bf-tooling\\craft\\checkout-repository.py" --repository https://invent.kde.org/sysadmin/binary-factory-tooling.git --into "C:/Craft/BinaryFactory/"
			"""

			// Update Craft itself and make sure NSIS is installed
			bat """
				cd "C:\\Craft\\BinaryFactory\\"

				python craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c -i --options virtual.ignored=True --update craft
				if errorlevel 1 exit /b %errorlevel%

				python craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} nsis
				if errorlevel 1 exit /b %errorlevel%
			"""
		}

		stage('Cleaning Up Prior Builds') {
			// Ensure we have nothing left behind in the packaging workspace used by Craft
			powershell """
				cd "C:/Craft/BinaryFactory/"
				\$CRAFT_TMPDIR = python "craftmaster/Craftmaster.py" --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} -q --get "packageDestinationDir()" virtual/base
				rm "\$CRAFT_TMPDIR/*"
			"""

			// Make sure the build environment for this application is clean
			bat """
				cd "C:\\Craft\\BinaryFactory\\"
				python craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} ${craftProjectParams} --unmerge ${craftRebuildBlueprint}
				if errorlevel 1 exit /b %errorlevel%
			"""
		}

		stage('Installing Dependencies') {
			// Ask Craftmaster to ensure all the dependencies are installed for this application we are going to be building
			bat """
				cd "C:\\Craft\\BinaryFactory\\"
				python craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} ${craftProjectParams} --install-deps ${craftBuildBlueprint}
				if errorlevel 1 exit /b %errorlevel%
			"""
		}

		stage('Building') {
			// Actually build the application now
			bat """
				cd "C:\\Craft\\BinaryFactory\\"
				python craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} --no-cache ${craftProjectParams} ${craftRebuildBlueprint}
				if errorlevel 1 exit /b %errorlevel%
			"""
		}

		stage('Packaging') {
			// Now generate an installer for it
			powershell """
				cd "C:/Craft/BinaryFactory/"
				python craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} ${craftProjectParams} --package ${craftBuildBlueprint}
				if(\$LASTEXITCODE -ne 0) {exit \$LASTEXITCODE}
				if (\$${craftPackageAppx}) {
					python craftmaster/Craftmaster.py --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} --options "[Packager]PackageType=AppxPackager" ${craftProjectParams} --package ${craftBuildBlueprint}
				}
				if(\$LASTEXITCODE -ne 0) {exit \$LASTEXITCODE}
			"""

			// Copy it to the Jenkins workspace so it can be grabbed from there
			powershell """
				cd "C:/Craft/BinaryFactory/"
				\$CRAFT_TMPDIR = python "craftmaster/Craftmaster.py" --config "${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} -q --get "packageDestinationDir()" virtual/base

				copy "\$CRAFT_TMPDIR/*" "\$env:WORKSPACE"
				rm "\$CRAFT_TMPDIR/*"
			"""

			// Ensure gpg-agent and dbus-daemon are no longer around
			// For reasons unknown, these processes somehow end up running within the Binary Factory environment
			// If they're allowed to continue running, then later cleanup operations will fail and leave the builder setup broken (requiring manual intervention to fix)
			bat """
				taskkill /F /IM gpg-agent.exe
				taskkill /F /IM dbus-daemon.exe
				python "C:/Craft/BinaryFactory/craftmaster/Craftmaster.py" --config "C:/Craft/BinaryFactory/${craftmasterConfig}" --target ${craftPlatform} -c ${craftOptions} --clean-unused
				exit /b 0
			"""
		}

		stage('Capturing Package') {
			// Then we ask Jenkins to capture the generated installers as an artifact
			archiveArtifacts artifacts: '*.appx, *.appxupload, *.exe, *.7z, *.sha256', onlyIfSuccessful: true
		}
	}

	// As the Windows Slaves are permanent ones, we erase the Workspace as the last thing we do
	deleteDir()
}
}
